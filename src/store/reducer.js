import {DECODE_MESSAGE_SUCEESS, ENCODE_MESSAGE_SUCCESS} from "./action";

const initialState = {
    encodedMessage: null,
    decodedMessage: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
      case ENCODE_MESSAGE_SUCCESS:
          return {...state, encodedMessage: action.message.encoded};
      case DECODE_MESSAGE_SUCEESS:
          return {...state, decodedMessage: action.message.decoded};
      default:
          return state;
  }
};

export default reducer;