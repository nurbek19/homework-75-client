import axios from '../axios-api';

export const ENCODE_MESSAGE_SUCCESS = 'ENCODE_MESSAGE_SUCCESS';
export const DECODE_MESSAGE_SUCEESS = 'DECODE_MESSAGE_SUCEESS';

export const encodeMessageSuccess = message => {
    return {type: ENCODE_MESSAGE_SUCCESS, message};
};

export const decodeMessageSuccess = message => {
    return {type: DECODE_MESSAGE_SUCEESS, message};
};

export const encodeMessage = message => {
  return dispatch => {
      return axios.post('/encode', message).then(response => {
          dispatch(encodeMessageSuccess(response.data));
      });
  }
};

export const decodeMessage = message => {
    return dispatch => {
        return axios.post('/decode', message).then(response => {
            dispatch(decodeMessageSuccess(response.data));
        });
    }
};

