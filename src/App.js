import React, {Component} from 'react';
import {connect} from 'react-redux';

import './App.css';
import {decodeMessage, encodeMessage} from "./store/action";

class App extends Component {
    state = {
        password: '',
        encode: '',
        decode: ''
    };

    changeValue = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    encodeMessage = event => {
        event.preventDefault();

        const message = {
            password: this.state.password,
            message: this.state.encode
        };


        if(this.state.password !== '') {
            this.props.onEncodedMessage(message).then(() => {
                this.setState({decode: this.props.encodedMessage, encode: '', password: ''});
            });
        } else {
            alert('Fill password form!');
        }
    };

    decodeMessage = event => {
        event.preventDefault();

        const message = {
            password: this.state.password,
            message: this.state.decode
        };

        if(this.state.password !== '') {
            this.props.onDecodedMessage(message).then(() => {
                this.setState({encode: this.props.decodedMessage, decode: '', password: ''});
            });
        } else {
            alert('Fill password form!');
        }
    };


    render() {
        return (
            <div className="App">
                <form action="#">
                    <label>Decoded message
                        <textarea name="encode" cols="30" rows="10"
                                  value={this.state.encode}
                                  onChange={this.changeValue}
                        />
                    </label>
                    <label>Password
                        <input type="text" name="password" value={this.state.password}
                               onChange={this.changeValue} required={true}/>
                    </label>
                    <button onClick={this.encodeMessage}>Encode</button>
                    <button onClick={this.decodeMessage}>Decode</button>

                    <label>Encoded message
                        <textarea name="decode" cols="30" rows="10"
                                  value={this.state.decode}
                                  onChange={this.changeValue}
                        />
                    </label>
                </form>
            </div>
        );
    }
}

const mapToProps = state => {
    return {
        encodedMessage: state.encodedMessage,
        decodedMessage: state.decodedMessage
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onEncodedMessage: message => dispatch(encodeMessage(message)),
        onDecodedMessage: message => dispatch(decodeMessage(message))
    }
};

export default connect(mapToProps, mapDispatchToProps)(App);
